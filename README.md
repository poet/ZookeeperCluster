# ZookeeperCluster
 zookeeper单机伪集群配置示例

# 运行
```
sh start.sh
```

# 停止
```
sh stop.sh
```

# 在`dubbo`中的配置
```
dubbo.registry.address=zookeeper://127.0.0.1:2181?backup=127.0.0.1:2182,127.0.0.1:2183
```

